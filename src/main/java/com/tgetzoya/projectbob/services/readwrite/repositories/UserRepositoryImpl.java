package com.tgetzoya.projectbob.services.readwrite.repositories;

import com.datastax.driver.core.*;
import com.datastax.driver.core.querybuilder.Insert;
import com.datastax.driver.core.querybuilder.QueryBuilder;
import com.tgetzoya.projectbob.models.LoginModel;
import com.tgetzoya.projectbob.models.NameModel;
import com.tgetzoya.projectbob.models.UserModel;
import com.tgetzoya.projectbob.models.error.ErrorModel;
import com.tgetzoya.projectbob.services.readwrite.exceptions.ErrorException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.UUID;

import static com.datastax.driver.core.querybuilder.QueryBuilder.eq;

/**
 * Implementation of database communications.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Repository
public class UserRepositoryImpl implements UserRepository {
    /**
     * The cluster to connect to.
     */
    private Cluster mCluster;
    /**
     * The session for the cluster to read and write from.
     */
    private Session mSession;

    /**
     * Makes sure the cluster and session are set.
     */
    @PostConstruct
    public void init() {
        mCluster = Cluster.builder().addContactPoint("192.168.1.159").build();
        mSession = mCluster.connect();
    }

    /**
     * Returns the user data from the supplied email address or internal id.
     *
     * @param user the UserModel object with either an email address or id
     * @return user data or an empty {@link com.tgetzoya.projectbob.models.ResponseModel} if no user exists.
     * @throws ErrorException if an error happens while retrieving the user.
     */
    @Override
    public UserModel getUser(UserModel user) throws ErrorException {
        UUID userId;

        if (null != user.getId()) {
            userId = user.getId();
        } else if (!StringUtils.isBlank(user.getEmail())) {
            userId = UUID.nameUUIDFromBytes(user.getEmail().getBytes());
        } else {
            throw new ErrorException(new ErrorModel("Either id or userId must be supplied."));
        }

        Statement statement = QueryBuilder.select()
                .all()
                .from("tgetzoya", "users")
                .where(eq("id", userId));

        ResultSet results = mSession.execute(statement);
        UserModel storedUser = null;
        NameModel name;

        for (Row row : results) {
            storedUser = new UserModel();
            storedUser.setId(row.getUUID("id"));
            storedUser.setEmail(row.getString("email"));
            storedUser.setPassword(row.getBytes("password"));
            storedUser.setSalt(row.getBytes("salt"));

            name = new NameModel();
            name.setFirst(row.getString("name_first"));
            name.setLast(row.getString("name_last"));
            name.setMiddle(row.getString("name_middle"));
            name.setPrefix(row.getString("name_prefix"));
            name.setSuffix(row.getString("name_suffix"));

            storedUser.setName(name);
        }

        return storedUser;
    }

    /**
     * Inserts the new user data into the database.
     *
     * @param newUser the user data to write to the database
     * @return a UserModel with the new internal id
     * @throws ErrorException if an error happens while inserting the user into the database.
     */
    @Override
    public UserModel createUser(UserModel newUser) throws ErrorException {
        UUID newId = UUID.nameUUIDFromBytes(newUser.getEmail().getBytes());
        UserModel model = new UserModel();

        model.setId(newId);

        UserModel checkForUser = getUser(model);

        if (null != checkForUser) {
            throw new ErrorException(new ErrorModel("User <" + newUser.getEmail() + "> already exists."));
        }

        Insert insert = QueryBuilder
                .insertInto("tgetzoya", "users")
                .value("id", newId)
                .value("email", newUser.getEmail())
                .value("name_first", newUser.getName().getFirst())
                .value("name_last", newUser.getName().getLast())
                .value("name_middle", newUser.getName().getMiddle())
                .value("name_prefix", newUser.getName().getPrefix())
                .value("name_suffix", newUser.getName().getSuffix())
                .value("password", newUser.getPassword())
                .value("salt", newUser.getSalt());

        ResultSet results = mSession.execute(insert);

        if (!results.wasApplied()) {
            throw new ErrorException(new ErrorModel("Could not write to database."));
        }

        return model;
    }

    /**
     * Inserts the login data to the database.
     *
     * @param loginModel the LoginModel to insert to the database.
     * @return whether the insertion worked
     */
    @Override
    public boolean insertLoginData(LoginModel loginModel) {
        Insert insert = QueryBuilder
                .insertInto("tgetzoya", "user_meta")
                .value("user_id", loginModel.getId())
                .value("time_stamp", System.currentTimeMillis())
                .value("auth_token", loginModel.getAuthToken())
                .value("description", "User logged in.");

        ResultSet result = mSession.execute(insert);

        return result.wasApplied();
    }
}
