/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * UserRepository.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.readwrite.repositories;

import com.tgetzoya.projectbob.models.LoginModel;
import com.tgetzoya.projectbob.models.UserModel;
import com.tgetzoya.projectbob.services.readwrite.exceptions.ErrorException;

/**
 * Interface for reading and writing data to the database.
 */
public interface UserRepository {
    /**
     * Returns the user data from the supplied email address or internal id.
     *
     * @param user the UserModel object with either an email address or id
     * @return user data or an empty {@link com.tgetzoya.projectbob.models.ResponseModel} if no user exists.
     * @throws ErrorException if an error happens while retrieving the user.
     */
    UserModel getUser(UserModel user) throws ErrorException;

    /**
     * Inserts the new user data into the database.
     *
     * @param newUser the user data to write to the database
     * @return a UserModel with the new internal id
     * @throws ErrorException if an error happens while inserting the user into the database.
     */
    UserModel createUser(UserModel newUser) throws ErrorException;

    /**
     * Inserts the login data to the database.
     *
     * @param loginModel the LoginModel to insert to the database.
     * @return whether the insertion worked
     */
    boolean insertLoginData(LoginModel loginModel);
}
