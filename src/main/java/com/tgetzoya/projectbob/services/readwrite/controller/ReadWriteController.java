/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * ReadWriteController.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.readwrite.controller;

import com.tgetzoya.projectbob.models.LoginModel;
import com.tgetzoya.projectbob.models.ResponseModel;
import com.tgetzoya.projectbob.models.UserModel;
import com.tgetzoya.projectbob.services.readwrite.service.ReadWriteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * This controller handles all reading and writing endpoints.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@RestController
public class ReadWriteController {
    /**
     * The actual reading and writing service
     */
    @Autowired
    ReadWriteService readWriteService;

    /**
     * Returns the string "OK" if the service is working.
     *
     * @return String "OK" if it's working
     */
    @RequestMapping(value = "/healthCheck",
            method = RequestMethod.GET,
            produces = MediaType.TEXT_XML_VALUE)
    @ResponseBody
    public ResponseEntity healthCheck() {
        return readWriteService.healthCheck();
    }

    /**
     * Returns the user to the calling service.
     *
     * @param user a {@link UserModel} that should contain either the email
     *             address of the internal id.
     * @return the UserModel of the user, or an error response
     */
    @RequestMapping(value = "/getUser",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseModel getUser(@RequestBody UserModel user) {
        return readWriteService.getUser(user);
    }

    /**
     * Creates a user in the database.
     *
     * @param newUser the user data to be written to the database
     * @return the result of the insertion
     */
    @RequestMapping(value = "/createUser",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseModel createUser(@RequestBody UserModel newUser) {
        return readWriteService.createUser(newUser);
    }


    /**
     * Write the login information to the database.
     *
     * @param loginModel the login data
     * @return the ResponseModel
     */
    @RequestMapping(value = "/writeLoginData",
            method = RequestMethod.POST,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseModel writeLoginData(@RequestBody LoginModel loginModel) {
        return readWriteService.insertLoginData(loginModel);
    }
}
