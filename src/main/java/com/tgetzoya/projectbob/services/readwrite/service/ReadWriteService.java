/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * ReadWriteService.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.readwrite.service;


import com.tgetzoya.projectbob.models.LoginModel;
import com.tgetzoya.projectbob.models.ResponseModel;
import com.tgetzoya.projectbob.models.UserModel;
import org.springframework.http.ResponseEntity;

/**
 * Interface for reading and writing to a database.
 */
public interface ReadWriteService {
    /**
     * Returns a status of OK if the service is up and running.
     *
     * @return A status of OK if the service is running
     */
    ResponseEntity healthCheck();

    /**
     * Returns the stored user.
     *
     * @param id the UserModel of the user
     * @return the UserModel of the stored user
     */
    ResponseModel<UserModel> getUser(UserModel id);

    /**
     * Inserts a user into the database.
     *
     * @param model the UserModel to insert into the database.
     * @return the UserModel of the inserted user data
     */
    ResponseModel<UserModel> createUser(UserModel model);

    /**
     * Insert user login information
     *
     * @param loginModel the LoginModel to insert into the database
     * @return an empty ResponseModel with the result code
     */
    ResponseModel insertLoginData(LoginModel loginModel);
}
