/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * ReadWriteServiceImpl.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.readwrite.service;

import com.tgetzoya.projectbob.models.LoginModel;
import com.tgetzoya.projectbob.models.ResponseModel;
import com.tgetzoya.projectbob.models.UserModel;
import com.tgetzoya.projectbob.services.readwrite.exceptions.ErrorException;
import com.tgetzoya.projectbob.services.readwrite.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.Arrays;

/**
 * Implementation of read and writing to and from a database.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@Component
public class ReadWriteServiceImpl implements ReadWriteService {
    /**
     * The connection to the database repository.
     */
    @Autowired
    private UserRepository mRepository;

    /**
     * Returns a status of OK if the service is running.
     *
     * @return a status of OK if the service is running
     */
    @Override
    public ResponseEntity healthCheck() {
        return ResponseEntity.ok("{\"status\":\"ok\"}");
    }

    /**
     * Returns the stored user.
     *
     * @param user the UserModel of the user
     * @return the UserModel of the stored user
     */
    @Override
    public ResponseModel getUser(UserModel user) {
        ResponseModel response = new ResponseModel();

        try {
            UserModel getUser = mRepository.getUser(user);
            response.setResult(0);
            response.setResponseData(getUser);
        } catch (ErrorException rde) {
            response.setResult(1);
            response.setErrors(Arrays.asList(rde.getError()));
        }

        return response;
    }


    /**
     * Inserts a user into the database.
     *
     * @param newUser the UserModel to insert into the database.
     * @return the UserModel of the inserted user data
     */
    @Override
    public ResponseModel createUser(UserModel newUser) {
        ResponseModel response = new ResponseModel();

        try {
            UserModel createUser = mRepository.createUser(newUser);
            response.setResult(0);
            response.setResponseData(createUser);
        } catch (ErrorException rde) {
            response.setResult(1);
            response.setErrors(Arrays.asList(rde.getError()));
        }

        return response;
    }

    /**
     * Insert user login information
     *
     * @param loginModel the LoginModel to insert into the database
     * @return an empty ResponseModel with the result code
     */
    @Override
    public ResponseModel insertLoginData(LoginModel loginModel) {
        ResponseModel response = new ResponseModel();

        if (mRepository.insertLoginData(loginModel)) {
            response.setResult(0);
        } else {
            response.setResult(1);
        }

        return response;
    }
}
