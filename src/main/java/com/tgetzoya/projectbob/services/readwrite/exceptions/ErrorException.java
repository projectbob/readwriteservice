/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * ErrorException.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.readwrite.exceptions;

import com.tgetzoya.projectbob.models.error.ErrorModel;
import lombok.Data;

/**
 * An exception to throw when there are errors.  This contains the entire
 * {@link ErrorModel} to store.
 */
@Data
public class ErrorException extends Exception {
    /**
     * The error to hold.
     */
    private ErrorModel error;

    /**
     * Allows using the error message as the message and the model as the
     * container.
     *
     * @param error the error to throw
     */
    public ErrorException(ErrorModel error) {
        super(error.getMessage());
        this.error = error;
    }
}
